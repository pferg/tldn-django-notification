from django.conf import settings
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.translation import ugettext

from notification import backends


class EmailBackend(backends.BaseBackend):
    spam_sensitivity = 2

    def can_send(self, user, notice_type):
        can_send = super(EmailBackend, self).can_send(user, notice_type)
        if can_send and user.email and user.is_active:
            return True
        return False

    def deliver(self, recipient, sender, notice_type, extra_context):
        # TODO: require this to be passed in extra_context

        context = self.default_context()
        context.update({
            "recipient": recipient,
            "sender": sender,
            "notice": ugettext(notice_type.display),
        })
        context.update(extra_context)
        
        messages = self.get_formatted_messages((
            "short.txt",
            "full.txt",
            "full.html",
        ), notice_type.label, context)

        subject = "".join(render_to_string("notification/email_subject.txt", {
            "message": messages["short.txt"],
        }, context).splitlines())
        
        text_content = render_to_string("notification/email_body.txt", {
            "message": messages["full.txt"],
        }, context)
        
        html_content = render_to_string("notification/email_body.html", {
            "message": messages["full.html"],
        }, context)
        
        bcc = context.get('bcc')
        cc = context.get('cc')
        replyto = context.get('replyto')
        extra_to = context.get('extra_to', [])
        
        """
        On mailtrap, CC and Replyto displayed correctly on the headers.
        But I don't think mailtrap will generate a email to the cc/bcc person.
        So you won't see a separate email in mail trap to the cc/bcc-ed person.
        BCC is not even displayed on the header for some reason.
        But maybe that is by design (http://stackoverflow.com/questions/2750211/sending-bcc-emails-using-a-smtp-server/2750359#2750359)
        """
        
        msg = EmailMultiAlternatives(subject = subject, body = text_content, 
                                     from_email = settings.DEFAULT_FROM_EMAIL, 
                                     to = [recipient.email] + extra_to,
                                     cc = [cc] if cc else None,
                                     bcc = [bcc] if bcc else None,
                                     headers = {'Reply-To': replyto} if replyto else None)
        msg.attach_alternative(html_content, "text/html")
        msg.send()
